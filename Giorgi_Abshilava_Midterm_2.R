cars = mtcars

cars$Category <- cut(cars$qsec, breaks = c(14.5, 17, 21, 23),
                     labels = c("A", "B", "C"), right = F)
cars

mean(cars$hp[cars$cyl == 8]) - mean(cars$hp[cars$cyl == 4])

cars[cars$Category == "A" & cars$hp < 230,]

cars[cars$Category == "A" & cars$hp > 240,]

cars[cars$Category == "B" & cars$disp > 140 & cars$gear > 4,]

nrow(cars[cars$gear == 3 & cars$Category == "A",]) / nrow(cars[cars$Category == "A",]) * 100

cars[cars$Category == "B",]